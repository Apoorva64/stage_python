﻿# Fonctionnement du programme
# Calculs
Les calculs des graphs se font dans les classes qui héritent de la classe NumberGraph qui contient la logique de base pour calculer les valeurs sur l’axe x, le step du graph, le début et la fin. Il sert aussi à calculer le random dans update_with_random. 

![TextDescription automatically generated](images/graph_chart.png)

Pour créer un nouveau graph il suffit de créer une autre classe qui hérite de NumberGraph, ainsi que de mettre les valeurs de y du graph qui sont calculés dans la méthode update du graph. Attention il ne faut pas oublier d’appeler la méthode update de NumberGraph (NumberGraph.update()). Pour changer le random d’un graph il suffit d’écraser la méthode update_with_random par celle qui est voulu dans votre graph. 

Les fonctions pour calculer en un point les graphs sont dans functions.py qui sont ensuite importes dans le ficher graph_classes.py ou l’on a nos classes de graphs et on calcule en tout valeurs de x, les valeurs de y en utilisant les fonctions.
# Interface
Pour faire l’interface j’ai utilisé pyqt5 avec designer. En effet le fichier de l’interface est main_window.ui que je converti ensuite avec pyuic5 en main_window.py.

Ui_MainWindow est ensuite importé de main_window.py et est ainsi utilisé pour initialiser l’interface.

Il faut faire attention au fait qu’il y ait un custom widget qui est le mplWidget que j’utilise pour mettre matplotlib dans pyqt5.

# Inputs
Les inputs sont faits dans le fichier principal du programme main.py

Dans le init du MainWindow on y retrouve 2 dictionnaires :

- Inputs : contient les noms des variables en key, et les valeurs en item
- Inputs_objects : contient les noms des variables en key, et contient les objets d’input en key (principalement des line_edit)

La méthode handle_inputs prend les donnes des inputs_objects et les mets dans les inputs.

La méthode handle_inputs_visibility, comme sont nom l’indique change la visibilité des inputs de façon à voir ce que l’on peut changer et pas.

La méthode update fait le gros travail.

En effet elle est connectée au bouton update graphe et entrée, et mets à jour les inputs mais aussi les graphes. En effet en fonctions des boutons appuyés dans l’interface dans la catégorie graph settings, des graphs sont ajoutés dans la liste graph_to_update. On appelle par la suite la méthode update ou update_with_random des graphs et on les plots une par une sur le Canvas matplotlib.

# Accéder au projet
## Méthode 1 :
Vous pouvez accéder au projet sur <https://gitlab.com/Apoorva64/stage_python>.

Si vous m’envoyez un mail sur <appadooapoorva@gmail.com>, je pourrais vous mettre maintainer
## Méthode 2 :
Lien du drive contenant tous les fichiers : <https://drive.google.com/drive/folders/1EL57KkJGBrzNY0B4nQdyy3KDscmzbM-T?usp=sharing>





