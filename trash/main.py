import matplotlib.pyplot as plt
import numpy as np
import math

# input laser
input_laser_power = 1
input_laser_waist = 1
input_laser_I = (2 * input_laser_power) / (math.pi * input_laser_waist ** 2)

saturation_I = 1

s = input_laser_I / saturation_I  # parametre de staturation
n = 1  # η est un facteur de proportionnalit´e qui apparaˆıt entre le champ diffus´e et le dipˆole atomique
s = 1

total_intensity = n ** 2 * 0.5 * s / (1 + s)  # L’intensit´e totale

w_l = 0
"""la r´eponse d’un dipˆole classique `a une excitation laser de fr´equence ωL. C’est donc en r´egime forc´e 
un rayonnement monochromatique de fr´equence ωL"""

w_at = 0
"""frequence de resonance"""

G_b = 1
"""Une des originalit´es de l’´echantillon atomique est le caract`ere r´esonant de la
diffusion. Il est alors possible d’interpr´eter Γb**−1 comme le temps typique d’un evenement de diffusion, 
Γb designant la largeur naturelle de la transition"""

d_l = w_l - w_at

d_l = 0


def elastic_intensity(w):
    d = w - w_l  # δ = ω − ωL.
    result = total_intensity * d_l * (w - w_l)
    return result


def inelastic_intensity(w, s):
    d = w - w_l  # δ = ω − ωL.

    d_l_g_b = (d_l / G_b) ** 2
    d_g_b = (d / G_b) ** 2
    first_part = n ** 2 * (1 / G_b)
    second_part = (s ** 2) / (8 * math.pi * (1 + s + 4 * d_l_g_b))

    numerator_big_part = d_g_b + (s / 4) + 1

    denominator_big_part1 = (1 / 4) + s / 4 + d_l_g_b - 2 * d_g_b

    denominator_big_part2 = (5 / 4) + (s / 2) + d_l_g_b - d_g_b

    big_part = numerator_big_part / (denominator_big_part1 ** 2 + d_g_b * denominator_big_part2 ** 2)
    result = first_part * second_part * big_part
    return result


angle = np.arange(-20, 20, 0.1)
output = []


def update_graph(value):
    s = value
    output = [inelastic_intensity(x, s) for x in angle]
    return output


#
output=update_graph(1)
plt.plot(angle, output)
plt.show()

# ((1 / 4) + (1 / 4) * s + ((d_l ** 2) / (G_b ** 2)) - 2 * ((d ** 2) / (G_b ** 2))) ** 2
